# Building Skunkworks-setup.exe
1) Open `Installer Setup/setup.suf` with Setup Factory 9
2) Click the Gear Icon on the top menu to build. Or F7 works fine too
3) Select Web (Single File)
4) Make sure that `Skunkworks-setup.exe` is being built to the `Executables` folder
5) Click Next and the installer will be build. If the file exists it will ask to overwrite

# Updating Installer
## Updating existing files in installer
1) Place updated files inside of the `Distribution Files` folder
2) Build as directed above

## Adding new files to installer
1) Place new files inside the `Distribution File` folder
2) Open `Installer Setup/setup.suf`
3) Drag and drop the new files into the list of files
4) Build as directed above